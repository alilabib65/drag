'use strict';

var countdown;
var delay = 1000;
var session = 0;
var sessionSeconds = session * 60;
var timer = document.getElementById('timer'),
    startSessionButton = document.getElementById('start'),
    pauseSessionButton = document.getElementById('pause');

timer.innerHTML = session + ':00';
displaySession('', 'none', 'none');

function startCountdown(seconds) {
    var minutes = parseInt(seconds / 60);
    var remainingSeconds = seconds % 60;
    if (remainingSeconds < 10) remainingSeconds = '0' + remainingSeconds;
    timer.innerHTML = minutes + ':' + remainingSeconds;
}

function startSession() {
    timer.innerHTML = 'يرجى الإنتظار';
    clearInterval(countdown);
    countdown = setInterval(function() {
        startCountdown(sessionSeconds);
        sessionSeconds++;
    }, delay);
    displaySession('none', '', '');
}

function pauseSession() {
    clearInterval(countdown);
    displaySession('', 'none', 'none');
    startSessionButton.innerHTML = 'إنتهت الحصة';
    $("#start").attr("disabled", true);
    $("#start").addClass("dis-button");
}

function displaySession(start, pause) {
    startSessionButton.style.display = start;
    pauseSessionButton.style.display = pause;
}

startSessionButton.addEventListener('click', startSession);
pauseSessionButton.addEventListener('click', pauseSession);
// collapes section
$('.panel-collapse').on('show.bs.collapse', function () {
    $(this).siblings('.panel-heading').addClass('active');
});

$('.panel-collapse').on('hide.bs.collapse', function () {
    $(this).siblings('.panel-heading').removeClass('active');
});
// append section
$('.button-bar').on('click', function () {
    document.getElementById("colorpaletteFillSection").style.display = "none";
    document.getElementById("colorpaletteSection").style.display = "none";
});
// show section
$('.button-show-lesson').on('click', function () {
    $(".board-show-mobile").toggleClass("button-show-lesson-show");
    $(".board-right-section").toggleClass("button-show-lesson-show-small");
});
$('.image-link').on('click', function () {
    $(".board-show-mobile").toggleClass("button-show-lesson-show");
    $(".board-right-section").toggleClass("button-show-lesson-show-small");
});