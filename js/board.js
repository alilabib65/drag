function initSketchpad() {
    "use strict";
    var sketchpadEl = document.getElementById("sketchpad");
    var sketchpad = new Sketchpad({
        containerEl: sketchpadEl,
        features: {
            displayCrosshair: true,
            displayGrid: true
        },
        createPageConfig: {
            no: 1,
            backgroundColor: "rgba(255,255,255,1)"
        }
    });

    window.sketchpad = sketchpad;


    var colorpalette = new Colorpalette({
        containerEl: document.getElementById("colorpalette")
    }).on("change", function (e) {
        sketchpad.getCurrentTool().setColor(e.color.red, e.color.green, e.color.blue, e.color.alpha);
    });
    window.colorpalette = colorpalette;

    var colorpaletteFill = new Colorpalette({
        containerEl: document.getElementById("colorpaletteFill")
    }).on("change", function (e) {
        sketchpad.getCurrentTool().setFillColor(e.color.red, e.color.green, e.color.blue, e.color.alpha);
    });
    window.colorpaletteFill = colorpaletteFill;


    /**
     * Changes current tool
     * @param  {string} toolId  - tool id
     */
    function selectTool(toolId) {
        console.log("selectTool", toolId);
        sketchpad.setTool(toolId);

        document.querySelectorAll(".toolbar .button").forEach(function (el) {
            el.classList.remove("active");
        });
        document.getElementById("tool-" + toolId).classList.add("active");

        document.getElementById("size").style.display = "none";
        document.getElementById("colorpaletteSection").style.display = "none";
        document.getElementById("colorpaletteFillSection").style.display = "none";

        var tool = sketchpad.getCurrentTool();
        /**
         * set toolbar for tool
         */

        if (typeof tool.getColor === "function") {
            colorpalette.setColor(tool.getColor(), "noPropagate");
        }

        if (typeof tool.getFillColor === "function") {
            colorpaletteFill.setColor(tool.getFillColor(), "noPropagate");
        }

        if (typeof tool.getSize === "function") {
            var size = tool.getSize();
            document.getElementById("size-slider").value = size;
        }

        switch (toolId) {
            case "pen":
                document.getElementById("colorpaletteSection").style.display = "block";
                document.getElementById("size").style.display = "block";
                break;
            case "highlighter":
                document.getElementById("colorpaletteSection").style.display = "block";
                document.getElementById("size").style.display = "block";
                break;
            case "eraser":
                break;
            case "rectangle":
                document.getElementById("colorpaletteFillSection").style.display = "block";
                document.getElementById("size").style.display = "block";
                break;
            case "circle":
                document.getElementById("colorpaletteFillSection").style.display = "block";
                document.getElementById("size").style.display = "block";
                break;
            case "line":
                document.getElementById("colorpaletteSection").style.display = "block";
                document.getElementById("size").style.display = "block";
                break;
        }
    }

    selectTool("pen");

    document.getElementById("size-slider").addEventListener("change", function (e) {
        if (typeof sketchpad.getCurrentTool().setSize === "function") {
            sketchpad.getCurrentTool().setSize(e.target.value);
        }
    });


    /**
     * Load sketch from json
     */
    function jsonToDraw(sketchpad, inputList) {
        var i,
            input;

        sketchpad.reset();
        sketchpad.receiveMessageFromServer({data: JSON.stringify({cmd: "history-begin"})});
        sketchpad.sendMessageToServer({cmd: "history-begin"});

        for (i = 0; i < inputList.length; i += 1) {
            input = inputList[i];
            input.bid = 0;
            input.uid = sketchpad.UID;
            if (input.config && input.config.sid) {
                console.log("PAGE: Input.cmd", input.cmd, input.config, input.config.sid);
            } else {
                console.log("Input: Input.cmd", input.cmd, input.sid);
            }

            sketchpad.sendMessageToServer(inputList[i]);
            sketchpad.receiveMessageFromServer({data: JSON.stringify(inputList[i])});
        }
        sketchpad.receiveMessageFromServer({data: JSON.stringify({cmd: "history-end"})});
        sketchpad.sendMessageToServer({cmd: "history-end"});
        //select current page?
        return inputList;
    }

    //pen
    document.getElementById('tool-pen').addEventListener("click", function () {
        selectTool("pen");
    });

    // marker
    document.getElementById('tool-highlighter').addEventListener("click", function () {
        selectTool("highlighter");
    });

    //eraser
    document.getElementById('tool-eraser').addEventListener("click", function () {
        selectTool("eraser");
    });


    //cutout
    document.getElementById('tool-cutout').addEventListener("click", function () {
        selectTool("cutout");
    });

    document.getElementById('tool-rectangle').addEventListener("click", function () {
        selectTool("rectangle");
    });

    document.getElementById('tool-line').addEventListener("click", function () {
        selectTool("line");
    });

    document.getElementById('tool-circle').addEventListener("click", function () {
        selectTool("circle");
    });

    document.getElementById('tool-move-viewport').addEventListener("click", function () {
        selectTool("move-viewport");
    });

    document.getElementById('tool-undo').addEventListener("click", function () {
        sketchpad.undo();
    });
    document.getElementById('tool-redo').addEventListener("click", function () {
        sketchpad.redo();
    });

}
initSketchpad();
$(document).ready(function () {
    $('.image-link img').on('click', function () {
        var url= $(this).attr("src");
        $('.card-images ul').append('<li>' +
            '<div class="loader-image second-loader">' +
            '<div class="loader-d">' +
            '<div class="d1"></div>'+
            '<div class="d2"></div>' +
            '<div class="d3"></div>' +
            '<div class="d4"></div>' +
            '<div class="d5"></div>' +
            '</div>' +
            '</div>' +
            '<i class="fa fa-close close-small">' +
            '</i>' +
            '<img src="' + url + '" class="img-responsive"/>' +
            '</li>');
        $('#sketchpad').append('<div class="large-div">' +
            '<div class="loader-image third-loader">' +
            '<div class="loader-d">' +
            '<div class="d1"></div>'+
            '<div class="d2"></div>' +
            '<div class="d3"></div>' +
            '<div class="d4"></div>' +
            '<div class="d5"></div>' +
            '</div>' +
            '</div>' +
            '<img src="' + url + '"  class="img-sketch img-responsive"/>' +
            '</div>' +
        '<i class="fa fa-close close-large">' +
        '</i> ');
        setTimeout(function() {
            $(".second-loader").fadeOut();
            $(".third-loader").fadeOut();
        }, 1000);
    });
    $('.color-change').on('click', function () {
        $(".color-section").toggleClass("hide-section");
    });
    $('ul').delegate('.close-small','click',function() {
        $(this).parent().addClass("hide-section");
    });
    $('#sketchpad').delegate('.close-large','click',function() {
        $("#sketchpad .fa-close").addClass("hide-section");
        $(".img-sketch").addClass("hide-section");
    });
    setTimeout(function() {
        $(".first-loader").fadeOut();
    }, 1000);
    $('.card-images').delegate('img','click',function() {
        var src= $(this).attr("src");
        $('#sketchpad').append('<div class="large-div">' +
            '<div class="loader-image third-loader">' +
            '<div class="loader-d">' +
            '<div class="d1"></div>'+
            '<div class="d2"></div>' +
            '<div class="d3"></div>' +
            '<div class="d4"></div>' +
            '<div class="d5"></div>' +
            '</div>' +
            '</div>' +
            '<img src="' + src + '"  class="img-sketch img-responsive"/>' +
            '</div>' +
            '<i class="fa fa-close close-large">' +
            '</i> ');
        setTimeout(function() {
            $(".third-loader").fadeOut();
        }, 1000);
    });
});