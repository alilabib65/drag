$(window).on("load", function () {
    $("#preloader").delay(600).fadeOut()
    $("body").css("overflow-y", "scroll")
}), $(document).ready(function () {
    (new WOW).init(), $("body").scrollspy({
        target: "#nav",
        offset: $(window).height() / 2
    }), $("#nav .nav-collapse").on("click", function () {
        $("#nav").toggleClass("open")
    }), $(".has-dropdown a").on("click", function () {
        $(this).parent().toggleClass("open-drop")
    }), $(window).on("scroll", function () {
        var o = $(this).scrollTop();
        o > 1 ? $("#nav").addClass("fixed-nav") : $("#nav").removeClass("fixed-nav"), o > 200 ? $("#back-to-top").fadeIn() : $("#back-to-top").fadeOut()
    })
});
// slider
$(document).ready(function () {
    var owl = $('.owl-carousel-first');
    owl.owlCarousel({
        lazyLoad: true,
        rtl: true,
        nav: true,
        dots: true,
        loop: true,
        autoplay: true,
        animateOut: "fadeOut",
        responsive: {
            0: {
                items: 1,
            },
            600: {
                items: 1
            },
            1000: {
                items: 1
            }
        }
    })
});

// lazy load
lozad(".lozad", {
    load: function (o) {
        o.src = o.dataset.src, o.onload = function () {
            o.classList.add("show-lazy")
        }
    }
}).observe();
// menu
$(".nav__menu-item").on("click", function () {
    $(this).find(".nav__submenu").toggleClass("open")
})
// search
$(document).ready(function () {
    var submitIcon = $('.sb-icon-search');
    var searchBox = $('.sb-search');
    var isOpen = false;

    submitIcon.mouseup(function () {
        return false;
    });
    searchBox.mouseup(function () {
        return false;
    });
    submitIcon.click(function () {
        if (isOpen == false) {
            searchBox.addClass('sb-search-open');
            isOpen = true;
            if ($(window).width() > 991) {
                $(".clock").toggleClass("hide-section");
            }
            $(".first-icon").toggleClass("hide-section");
            $(".second-icon").toggleClass("hide-section");
        } else {
            searchBox.removeClass('sb-search-open');
            isOpen = false;
            if ($(window).width() > 991) {
                $(".clock").toggleClass("hide-section");
            }
            $(".first-icon").toggleClass("hide-section");
            $(".second-icon").toggleClass("hide-section");
        }
    });
});
// news section
$('.ticker').marquee({
    duration: 15000,
    gap: 50,
    duplicated: true,
    pauseOnHover: true
});
// play video
$(document).on('click', '.js-videoPoster', function (ev) {
    ev.preventDefault();
    var $poster = $(this);
    var $wrapper = $poster.closest('.js-videoWrapper');
    videoPlay($wrapper);
});

function videoPlay($wrapper) {
    var $iframe = $wrapper.find('.js-videoIframe');
    var src = $iframe.data('src');
    $wrapper.addClass('videoWrapperActive');
    $iframe.attr('src', src);
}

function videoStop($wrapper) {
    if (!$wrapper) {
        var $wrapper = $('.js-videoWrapper');
        var $iframe = $('.js-videoIframe');
    } else {
        var $iframe = $wrapper.find('.js-videoIframe');
    }
    $wrapper.removeClass('videoWrapperActive');
    $iframe.attr('src', '');
}
// slider two
$(document).ready(function () {
    var owl_two = $('.owl-carousel-two');
    owl_two.owlCarousel({
        lazyLoad: true,
        rtl: true,
        nav: true,
        items: 4,
        margin: 30,
        dots: false,
        loop: true,
        animateOut: "fadeOut",
        responsive: {
            0: {
                items: 1,
                dots: true,
            },
            600: {
                items: 3,
                dots: true,
            },
            1000: {
                items: 4,
                dots: true
            }
        }
    })
});
// slider on mobile
$(".news-headlines .new-vir").on("click", function () {
    $(".new-popup").toggleClass("show-section");
});
$(".close-icon").on("click", function () {
    $(".new-popup").toggleClass("show-section");
});
// slider three
$(document).ready(function () {
    var owl_two = $('.owl-carousel-three');
    owl_two.owlCarousel({
        lazyLoad: true,
        rtl: true,
        nav: true,
        items: 3,
        margin: 30,
        dots: false,
        loop: true,
        animateOut: "fadeOut",
        responsive: {
            0: {
                items: 1,
                dots: true,
            },
            600: {
                items: 2,
                dots: true,
            },
            1000: {
                items: 3,
                dots: true
            }
        }
    })
});
// kyas page script
var tabsFn = (function () {
    function init() {
        setHeight();
    }

    function setHeight() {
        var $tabPane = $('.tab-pane'),
            tabsHeight = $('.nav-tabs').height();
        $tabPane.css({
            height: tabsHeight
        });
    }
    $(init);
})();
// show table
$(".mobile-category").on("click", function () {
    $(".nav-mobile").addClass("show-div");
    $("body").addClass("over-n");
});
$(".nav-mobile i").on("click", function () {
    $(".nav-mobile").removeClass("show-div");
    $("body").removeClass("over-n");
});
$(".nav-tabs li").on("click", function () {
    $(".nav-mobile").removeClass("show-div");
    $("body").removeClass("over-n");
});
// date picker
// show password
// select2
$(document).ready(function () {
    $('.img-eyes1').on('click', function () {
        var $inp1 = $(this).parent().find(".input-password");
        $inp1.attr('type') === 'password' ? $inp1.attr('type', 'text') : $inp1.attr('type', 'password');
    });
    $('.date input').datepicker({
        autoclose: true,
    });
    $('.js-states-gender').select2({
        minimumResultsForSearch: Infinity
    });
    $('.js-example-basic-single').select2();
});
// tabs on ask result page
$(document).ready(function() {
    (function ($) {
        $('.tab-ask ul.tabs-ask').addClass('active').find('> li:eq(0)').addClass('current');

        $('.tab-ask ul.tabs-ask li a').click(function (g) {
            var tab = $(this).closest('.tab-ask'),
                index = $(this).closest('li').index();
            tab.find('ul.tabs-ask > li').removeClass('current');
            $(this).closest('li').addClass('current');
            tab.find('.tab_content-ask').find('div.tabs_item-ask').not('div.tabs_item-ask:eq(' + index + ')').slideUp();
            tab.find('.tab_content-ask').find('div.tabs_item-ask:eq(' + index + ')').slideDown();
            g.preventDefault();
        } );
    })(jQuery);
});
// tabs on  training page
$(document).ready(function() {
    (function ($) {
        $('.tab-training ul.tabs-training').addClass('active').find('> li:eq(0)').addClass('current-training');
        $('.tab-training ul.tabs-training li .a-tabs').click(function (g) {
            var tab = $(this).closest('.tab-training'),
                index = $(this).closest('li').index();
            tab.find('ul.tabs-training > li').removeClass('current-training');
            $(this).closest('li').addClass('current-training');
            tab.find('.tab_content-training').find('div.tabs_item-training').not('div.tabs_item-training:eq(' + index + ')').slideUp();
            tab.find('.tab_content-training').find('div.tabs_item-training:eq(' + index + ')').slideDown();
            g.preventDefault();
        } );
    })(jQuery);
});
// follow btn

$(document).ready(function(){
    $('.follow').click(function(){
        if($(this).hasClass('followed')){
            $(this).removeClass('followed');
            $(this).find('img').attr('src', 'img/icon/follow-user.png');
            $(this).find('span').text('متابعة');
        }else if($(this).hasClass('cancel')) {
            $(this).removeClass('followed');
            $(this).find('img').attr('src', 'img/icon/follow-user.png');
            $(this).find('span').text('متابعة');
        }
        else{
            $(this).addClass('followed');
            $(this).find('img').attr('src', 'img/icon/user.png');
            $(this).find('span').text('متابع');
        }
    });

    $('.follow').mouseenter(function(){
        if($(this).hasClass('followed')){
            $(this).addClass('cancel');
            $(this).find('img').attr('src', 'img/icon/unfollow.png');
            $(this).find('span').text('إلغاء المتابعة');
        }
        else{
            $(this).removeClass('cancel').removeClass('followed');
            $(this).find('img').attr('src', 'img/icon/follow-user.png');
            $(this).find('span').text('متابعة');
        }
    });

    $('.follow').mouseleave(function(){
        if($(this).hasClass('cancel')){
            console.log('has cancel');
            $(this).removeClass('cancel').addClass('followed');
            $(this).find('img').attr('src', 'img/icon/user.png');
            $(this).find('span').text('متابع');
        }
        else{
            $(this).removeClass('cancel').removeClass('followed');
            $(this).find('img').attr('src', 'img/icon/follow-user.png');
            $(this).find('span').text('متابعة');
        }
    })

    // image preview
    function readURL(input) {

        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $(input).next().attr('src', e.target.result);
            };

            reader.readAsDataURL(input.files[0]);
        }
    }
    $(".joint").change(function () {
        readURL(this);
    });
    // exam block
    $(".download").click(function () {
        $(".show-block").addClass("hidden-block");
        $(".after-download").addClass("show-block");
        $("#e-exam-tab").attr("disabled", true);
        $("#e-exam-tab").addClass("dis-button");
        $(".hidden-block").removeClass("show-block");
        $(".after-download").removeClass("hidden-block");
    });
});

//   timer
$(document).ready(function () {

    $('.download').click(function (e) {
        e.preventDefault();

        //COUNTDOWN TIMER
        //edit ".25" below to change time in terms of day

        var act_hours = 1 * 60;
        var act_mins = 1 * 60;
        var deadline = new Date(Date.now() + act_hours * act_mins * 1000);

        var x = setInterval(function () {

            var now = Date.now();
            var t = deadline - now;
            // var days = Math.floor(t / (1000 * 60 * 60 ));
            var hours = Math.floor((t % (1000 * act_mins * act_hours )) / (1000 * 60 * 60));
            var minutes = Math.floor((t % (1000 * 60 * 60)) / (1000 * 60));
            var seconds = Math.floor((t % (1000 * act_mins)) / 1000);
            // document.getElementById("day").innerHTML = days;
            document.getElementById("hour").innerHTML = hours;
            document.getElementById("minute").innerHTML = minutes;
            document.getElementById("second").innerHTML = seconds;
            if (t < 0) {
                clearInterval(x);
                // document.getElementById("day").innerHTML = '0';
                document.getElementById("hour").innerHTML = '0';
                document.getElementById("minute").innerHTML = '0';
                document.getElementById("second").innerHTML = '0';
            }

            $('.upload-wrapper input[type="file"]').change(function(){
                clearInterval(x);
                $(".upload-wrapper .file-upload").addClass("hide-section");
                $(".btn-upload").removeClass("hide-section");
                $(".upload-wrapper").addClass("new-upload");
            });


        }, 1000);

        //COUNTDOWN BAR

        function progress(timeleft, timetotal, $element) {
            var progressBarWidth = timeleft * $element.width() / timetotal;
            $element.find('div').animate({
                width: progressBarWidth
            }, timeleft == timetotal ? 0 : 1000, 'linear');
            if (timeleft > 0) {
                setTimeout(function () {
                    progress(timeleft - 1, timetotal, $element);
                }, 1000);
            }
        };
        //adjust these numbers to match time set
        //must be in seconds
        progress(1740, 1740, $('#progressBar'));
    });

    $('.sumbit-file').click(function(){

    });

    // show file name
    $('.upload-wrapper input[type="file"]').change(function(e){
        var fileName = e.target.files[0].name;
        $(this).parent().find('span').text(fileName);
    });

});
// pop up
var numOfPieces = 6 * 6;
var frag = document.createDocumentFragment();
function insertInnerPieces($el, innerPieces) {
    for (var i = 0; i < innerPieces; i++) {
        var $inner = document.createElement('div');
        $inner.classList.add('popup__piece-inner');
        $el.appendChild($inner);
    }
};
for (var i = 1; i <= numOfPieces; i++) {
    var $piece = document.createElement('div');
    $piece.classList.add('popup__piece');
    insertInnerPieces($piece, 3);
    frag.appendChild($piece);
}
document.querySelector('.popup__pieces').appendChild(frag);
var $popupsCont = document.querySelector('.popups-cont');
var $popup = document.querySelector('.popup');
var $board = document.querySelector('.all-board');
var popupAT = 900;
document.querySelector('.popup-btn').addEventListener('click', function() {
    $board.classList.add('blur-section');
    $popupsCont.classList.add('s--popup-active');
    $popup.classList.add('s--active');
});
function closeHandler() {
    $board.classList.remove('blur-section');
    $popupsCont.classList.remove('s--popup-active');
    $popup.classList.remove('s--active');
    $popup.classList.add('s--closed');

    setTimeout(function() {
        $popup.classList.remove('s--closed');
    }, popupAT);
}
document.querySelector('.popup__close').addEventListener('click', closeHandler);
document.querySelector('.popups-cont__overlay').addEventListener('click', closeHandler);